set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'vim-airline/vim-airline'
Plugin 'derekwyatt/vim-fswitch'
Plugin 'kien/ctrlp.vim'
Plugin 'tpope/vim-commentary'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'majutsushi/tagbar'
Plugin 'mkitt/tabline.vim.git'
Plugin 'airblade/vim-gitgutter'
Plugin 'kshenoy/vim-signature'
Plugin 'calviken/vim-gdscript3'
Plugin 'ervandew/supertab'

"Themes
Plugin 'altercation/solarized'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'dracula/vim'
Plugin 'blockloop/vim-codeschool'
Plugin 'drewtempelmeyer/palenight.vim'
Plugin 'altercation/vim-colors-solarized'
Plugin 'sickill/vim-monokai'
Plugin 'kristijanhusak/vim-hybrid-material'
Plugin 'stillwwater/vim-nebula'

call vundle#end()            " required
filetype plugin indent on    " required

" Airline
let g:airline_theme='deus'

" CtrlP 
"let g:ctrlp_map = '<c-p>'
"let g:ctrlp_cmd = 'CtrlP'


" FSwitch
" This plugin makes switching easy between source and header files

au! BufEnter *.cc let b:fswitchdst = 'h' | let b:fswitchlocs='reg:/src/include'
au! BufEnter *.h let b:fswitchdst = 'cpp,cc' | let b:fswitchlocs='reg:/include/src'


syntax on
"set ic
set hlsearch
set nu
set expandtab 
set mouse=a

" Very important, to keep undo history even when changing buffer
set hidden

" Classical clipboard behaviour
set clipboard=unnamed
" Classical backspace 
set bs=2
"Allow using backspace in insert mode"
set backspace=indent,eol,start

set tabstop=4
set shiftwidth=4


" code block indent without changing the current selection
vnoremap < <gv
vnoremap > >gv

set smartindent

"Display line/column"
set ruler

" width of document
"set tw=79
"set colorcolumn=80
"
function ToggleColorMode()
    if &background == "dark"
        "echom "current scheme is dark"
        set background=light
        colorscheme morning
    else
        "echom "current scheme is light"
        set background=dark
        colorscheme palenight
    endif
endfunction


" Setting <Leader> on spacebar key
let mapleader=" "
" Toggling search hilight
nmap <C-H> :set hls! <cr>
" Jumping to header/source file
nmap <Leader>h :FSHere<cr>
" Nerdtree
nmap <Leader>n :NERDTreeToggle<cr>
" Tagbar
nmap <Leader>t :TagbarToggle<cr>
nmap <Leader><Leader> :b#<cr>
" Toggle color scheme
nmap <Leader>c :call ToggleColorMode()<cr>


if $TERM == 'xterm-256color'
    set t_Co=256
endif

if (has("termguicolors"))
      set termguicolors
endif

set background=dark
colorscheme palenight
set t_ut=

"hide toolbar
set guioptions-=T 
"hide menubar
set guioptions-=m



" Settings according to filetypes

autocmd FileType make set noexpandtab shiftwidth=4 softtabstop=0
autocmd FileType *.cc set expandtab shiftwidth=4 softtabstop=0 foldmethod=syntax foldlevel=1

" Some useful shortcuts
" CHEAT SHEET
" C-O : jump to previously visited locations
" C-I : jump to nextly visited locations
" ci) : delete everything inside parenthesis scope
" :! command % (% is the current file), example   :! ctags -R .
" gc : comment out selection/direction (vim-commentary plugin)
" gcc : comment out current line
" ]} : go to end of code block  
" [{ : go to start of code block
" % : go to end of current () scope
" zf : create fold
" za : toggle fold
" zR : Reset (zero) folding
